import os
import yaml
import logging.config


class SetupLogging:
    def __init__(self):
        self.default_config = os.path.join(os.path.dirname(
            os.path.abspath('__file__')), "./resources/logging.yaml")

    def setup_logging(self, default_level=logging.INFO, env_key='LOG_CFG'):
        path = os.getenv(env_key, self.default_config)
        if os.path.exists(path):
            with open(path, 'r') as f:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
        else:
            print('Error in loading logging configuration. Using default configs')
            logging.basicConfig(level=default_level)
