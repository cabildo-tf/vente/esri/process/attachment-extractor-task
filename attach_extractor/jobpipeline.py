import re
from attach_extractor.job import JobFactory
from attach_extractor.utils import Convert


def fill_jobs(mimetypes):
    jobs = {}
    for k, config_jobs in mimetypes.items():
        jobs[k] = []
        for config_job in config_jobs:
            jobs[k].append(JobFactory.create_extractor(config_job))
    return jobs


class JobPipeline(object):
    def __init__(self, mimetypes, feature_fieldnames, attach_fieldnames):
        self.__feature_convert = Convert(keys=feature_fieldnames)
        self.__attach_convert = Convert(keys=attach_fieldnames)
        self.__jobs = fill_jobs(mimetypes)
        self.validate_configuration()

    def validate_configuration(self):
        for k, operations in self.__jobs.items():
            for op in operations:
                op.validate(self.__feature_convert.get_fieldnames(), self.__attach_convert.get_fieldnames())

    def run(self, feature, attach):
        if type(feature) is not dict:
            feature = self.__feature_convert.convert_to_dictionary(feature)
            attach = self.__attach_convert.convert_to_dictionary(attach)

        feature, attach = self.run_pipeline(feature, attach)
        return self.__feature_convert.convert_to_array(feature), self.__attach_convert.convert_to_array(attach)

    def run_pipeline(self, feature, attach):
        content_type = attach['content_type']
        for k, operations in self.__jobs.items():
            if re.match(k, content_type):
                for op in operations:
                    feature, attach = op.run(feature, attach)
            else:
                raise Exception("Attachment type '{}' is not recognized")

        return feature, attach
