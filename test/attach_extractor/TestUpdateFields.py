import unittest
import io
from nose.tools import eq_, ok_
from PIL import Image, ImageChops
from attach_extractor.job import UpdateFieldJob


def load_image(path):
    with open(path, "rb") as image:
        f = image.read()
        return memoryview(bytearray(f))


class TestUpdateFieldJob(unittest.TestCase):
    def setUp(self):
        self.feature = {
            'nombre_fichero': 'hola.jpg',
            'url_base': None
        }
        self.attach = {
            'rel_globaid': '{4EA25197-A13C-4B87-B145-9C4C2C31F6D8}',
            'data': load_image('test/resources/test.jpg'),
            'att_name': 'test.jpg'
        }

    def test_returnFilledField_when_passAFieldname(self):
        config_job = {
            'field': 'nombre_fichero',
            'value': '{att_name}'
        }

        job = UpdateFieldJob(**config_job)
        feature, attach = job.run(self.feature.copy(), self.attach.copy())

        for key, value in self.feature.items():
            if key == 'nombre_fichero':
                value = 'test.jpg'
            eq_(feature[key], value)
        eq_(attach, self.attach)

    def test_returnReplacedText_when_passFormatString(self):
        config_job = {
            'field': 'url_base',
            'value': 'https://apd31lv.teide.int/mediastorage/images/{att_name}'
        }

        job = UpdateFieldJob(**config_job)
        feature, attach = job.run(self.feature.copy(), self.attach.copy())

        for key, value in self.feature.items():
            if key == 'url_base':
                value = 'https://apd31lv.teide.int/mediastorage/images/test.jpg'
            eq_(feature[key], value)
        eq_(attach, self.attach)

    def test_returnException_when_passFormatStringWithUnkownKey(self):
        config_job = {
            'field': 'url_base',
            'value': 'https://apd31lv.teide.int/mediastorage/images/{name}'
        }

        job = UpdateFieldJob(**config_job)
        self.assertRaises(Exception, job.run, self.feature.copy(), self.attach.copy())

    def test_returnException_when_fieldnameNotExists(self):
        config_job = {
            'field': 'url_base',
            'value': 'https://apd31lv.teide.int/mediastorage/images/{name}'
        }

        job = UpdateFieldJob(**config_job)

        self.assertRaises(Exception, job.validate, list(self.feature.keys()), list(self.attach.keys()))
