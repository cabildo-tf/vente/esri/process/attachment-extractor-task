import setuptools

setuptools.setup(
    name="attach_extractor",
    version="0.0.1",
    author="Ignacio Lorenzo",
    author_email="nacholore@gmail.com",
    description="Extract attachments of ArcGIS Database",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
